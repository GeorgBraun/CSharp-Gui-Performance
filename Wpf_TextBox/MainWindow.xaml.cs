﻿// Super-Quellen für DispatcherTimer
// http://www.wpf-tutorial.com/misc/dispatchertimer/
// Prioritäten: http://www.abhisheksur.com/2011/03/all-about-net-timers-comparison.html

// BLOG über die verschiedenen Timer-Klassen:
// http://web.archive.org/web/20150329101415/https://msdn.microsoft.com/en-us/magazine/cc164015.aspx

// Verwandtes Thema:
// Windows Forms: Give Your .NET-Based Application a Fast and Responsive UI with Multiple Threads
// http://web.archive.org/web/20140430033105/http://msdn.microsoft.com/en-us/magazine/cc300429.aspx

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_TextBox {
  /// <summary>
  /// Interaktionslogik für MainWindow.xaml
  /// </summary>
  public partial class MainWindow:Window {

    int i;
    int Anz;  // Anzahl der Wiederholungen
    int tInt; // Inervallzeit des Timers in ms

    System.Windows.Threading.DispatcherTimer dpTimer;

    System.Diagnostics.Stopwatch StoppUhr;
    DateTime NowTick;

    public MainWindow() {
      InitializeComponent();
    }

    private void btnClear_Click(object sender, RoutedEventArgs e) {
      tbxAusg.Text="";
    }

    private void tbxAnz_TextChanged(object sender, TextChangedEventArgs e) {
      Anz = LeseTextBoxInt(tbxAnz, 2);
    }

    private void tbxTint_TextChanged(object sender, TextChangedEventArgs e) {
      tInt = LeseTextBoxInt(tbxTint, 1);
    }

    private void btnDptStringPlusStart_Click(object sender, RoutedEventArgs e) {
      DpTimerStringPlusStart();
    }

    private void btnDptStringPlusStopp_Click(object sender, RoutedEventArgs e) {
      DpTimerStringPlusStop();
    }

    private void btnDptAppendStart_Click(object sender, RoutedEventArgs e) {
      DpTimerAppendStart();
    }

    private void btnDptAppendStopp_Click(object sender, RoutedEventArgs e) {
      DpTimerAppendStop();
    }

    #region DpTimerStringPlus
    public void DpTimerStringPlusStart() {
      // Start the timer
      i=0;

      // Initialize DispatcherTimer
      // Neuen DispatcherTimer erzeugen, Send ist maximale Priorität:
      dpTimer = new System.Windows.Threading.DispatcherTimer(System.Windows.Threading.DispatcherPriority.Send);
      dpTimer.Interval = TimeSpan.FromMilliseconds(tInt);
      dpTimer.Tick += DpTimerStringPlusTick;

      StoppUhr = new System.Diagnostics.Stopwatch();

      //MessageBox.Show("dpTimer.Interval: "+dpTimer.Interval.ToString());

      dpTimer.Start();
    }

    public void DpTimerStringPlusTick(object sender, EventArgs e) {
      if(!StoppUhr.IsRunning) { StoppUhr.Start(); }

      NowTick = DateTime.Now;

      tbxAusg.Text = "i="+i+"; NowTick="+NowTick.ToString(@"ss\,fff")+Environment.NewLine  + tbxAusg.Text;

      i++;

      if(i>=Anz) {
        dpTimer.Stop();
        StoppUhr.Stop();

        tbxAusg.Text = "FERTIG:"+Environment.NewLine+ResultText(StoppUhr.ElapsedMilliseconds, i)+Environment.NewLine  +tbxAusg.Text;
      }
    }

    public void DpTimerStringPlusStop() {
      if(dpTimer!=null && dpTimer.IsEnabled) {
        dpTimer.Stop();
        StoppUhr.Stop();
      }
    }
    #endregion DpTimerStringPlus

    #region DpTimerAppend
    public void DpTimerAppendStart() {
      // Start the timer
      i=0;

      // Initialize DispatcherTimer
      // Neuen DispatcherTimer erzeugen, Send ist maximale Priorität:
      dpTimer = new System.Windows.Threading.DispatcherTimer(System.Windows.Threading.DispatcherPriority.Send);
      dpTimer.Interval = TimeSpan.FromMilliseconds(tInt);
      dpTimer.Tick += DpTimerAppendTick;

      StoppUhr = new System.Diagnostics.Stopwatch();

      //MessageBox.Show("dpTimer.Interval: "+dpTimer.Interval.ToString());

      dpTimer.Start();
    }

    public void DpTimerAppendTick(object sender, EventArgs e) {
      if(!StoppUhr.IsRunning) { StoppUhr.Start(); }

      NowTick = DateTime.Now;

      tbxAusg.AppendText("i="+i+"; NowTick="+NowTick.ToString(@"ss\,fff")+Environment.NewLine);
      tbxAusg.ScrollToEnd();

      i++;

      if(i>=Anz) {
        dpTimer.Stop();
        StoppUhr.Stop();

        tbxAusg.AppendText("FERTIG:"+Environment.NewLine+ResultText(StoppUhr.ElapsedMilliseconds, i));
        tbxAusg.ScrollToEnd();
      }
    }

    public void DpTimerAppendStop() {
      if(dpTimer!=null && dpTimer.IsEnabled) {
        dpTimer.Stop();
        StoppUhr.Stop();
      }
    }
    #endregion DpTimerAppend

    #region SupportMethoden
    public string ResultText(long zeitMillisec, int anz) {
      return "t Ges: "+zeitMillisec+" ms; t INT AVG: "+(((double)zeitMillisec)/(anz-1)).ToString("F1")+" ms.";
    }

    public int LeseTextBoxInt(TextBox textBoxName, int? unterGrenze=null) {
      bool Pok;
      int  Erg;
      Pok = int.TryParse(textBoxName.Text, out Erg);
      if(!Pok) {
        // Keine gültige Zahl, funktioniert nur bei double, nicht bei int
        Erg = 0;
        textBoxName.Background = Brushes.LightPink;
      } else {
        if(unterGrenze!=null && Erg<unterGrenze) {
          textBoxName.Background = Brushes.LightPink;
        } else {
          textBoxName.Background = Brushes.White;
        }
      }
      return Erg;
    }
    #endregion SupportMethoden
  }
}
