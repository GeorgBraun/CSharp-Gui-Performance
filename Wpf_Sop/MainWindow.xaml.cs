﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_Sop {
  /// <summary>
  /// Interaktionslogik für MainWindow.xaml
  /// </summary>
  public partial class MainWindow:Window {

    int i;
    int Anz;  // Anzahl der Wiederholungen
    int tInt; // Inervallzeit des Timers in ms
    int Sid;  // Serien-ID des SOP
    double xleft, xright, xanz; // für x-Achse

    System.Windows.Threading.DispatcherTimer dpTimer;

    System.Diagnostics.Stopwatch StoppUhr;
    DateTime NowTick;

    public MainWindow() {
      InitializeComponent();
    }

    private void btnClear_Click(object sender, RoutedEventArgs e) {
      tbxAusg.Text="";
      Sop1.Series.Clear();
    }

    private void tbxAnz_TextChanged(object sender, TextChangedEventArgs e) {
      Anz = LeseTextBoxInt(tbxAnz, 2);
    }

    private void tbxTint_TextChanged(object sender, TextChangedEventArgs e) {
      tInt = LeseTextBoxInt(tbxTint, 1);
    }

    private void tbxAnzScroll_TextChanged(object sender, TextChangedEventArgs e) {
      xanz = LeseTextBoxInt(tbxAnzScroll, 1);
    }


    private void btnDptXallStart_Click(object sender, RoutedEventArgs e) {
      DpTimerXallStart();
    }

    private void btnDptXallStopp_Click(object sender, RoutedEventArgs e) {
      DpTimerXallStopp();
    }

    private void btnDptXscrollStart_Click(object sender, RoutedEventArgs e) {
      DpTimerXscrollStart();
    }

    private void btnDptXscrollStopp_Click(object sender, RoutedEventArgs e) {
      DpTimerXscrollStopp();
    }



    #region DpTimerXall
    public void DpTimerXallStart() {
      // Start the timer
      i=0;
      Sid = Sop1.Series.AddSeries("Xall "+Sop1.Series.Count);
      Sop1.XAxis.Scale.Auto=true;
      Sop1.YAxis.Scale.Auto=true;

      // Initialize DispatcherTimer
      // Neuen DispatcherTimer erzeugen, Send ist maximale Priorität:
      dpTimer = new System.Windows.Threading.DispatcherTimer(System.Windows.Threading.DispatcherPriority.Send);
      dpTimer.Interval = TimeSpan.FromMilliseconds(tInt);
      dpTimer.Tick += DpTimerXallTick;

      StoppUhr = new System.Diagnostics.Stopwatch();

      //MessageBox.Show("dpTimer.Interval: "+dpTimer.Interval.ToString());

      dpTimer.Start();
    }

    public void DpTimerXallTick(object sender, EventArgs e) {
      if(!StoppUhr.IsRunning) { StoppUhr.Start(); }

      NowTick = DateTime.Now;

      Sop1.Series[Sid].AddXY(i, NowTick.Millisecond);
      //tbxAusg.AppendText("i="+i+"; NowTick="+NowTick.ToString(@"ss\,fff")+Environment.NewLine);
      //tbxAusg.ScrollToEnd();

      i++;

      if(i>=Anz) {
        dpTimer.Stop();
        StoppUhr.Stop();

        tbxAusg.AppendText("FERTIG:"+Environment.NewLine+ResultText(StoppUhr.ElapsedMilliseconds, i)+Environment.NewLine);
        tbxAusg.ScrollToEnd();
      }
    }

    public void DpTimerXallStopp() {
      if(dpTimer!=null && dpTimer.IsEnabled) {
        dpTimer.Stop();
        StoppUhr.Stop();
      }
    }
    #endregion DpTimerXall


    #region DpTimerXscroll
    public void DpTimerXscrollStart() {
      // Start the timer
      i=0;
      Sid = Sop1.Series.AddSeries("Xscroll "+Sop1.Series.Count);

      // Initialize DispatcherTimer
      // Neuen DispatcherTimer erzeugen, Send ist maximale Priorität:
      dpTimer = new System.Windows.Threading.DispatcherTimer(System.Windows.Threading.DispatcherPriority.Send);
      dpTimer.Interval = TimeSpan.FromMilliseconds(tInt);
      dpTimer.Tick += DpTimerXscrollTick;

      StoppUhr = new System.Diagnostics.Stopwatch();

      //MessageBox.Show("dpTimer.Interval: "+dpTimer.Interval.ToString());

      dpTimer.Start();
    }

    public void DpTimerXscrollTick(object sender, EventArgs e) {
      if(!StoppUhr.IsRunning) { StoppUhr.Start(); }

      NowTick = DateTime.Now;

      Sop1.Series[Sid].AddXY(i, NowTick.Millisecond);

      xleft = i-xanz;
      xleft = (xleft) < 0 ? 0 : xleft;
      xright = xleft + xanz;
      Sop1.XAxis.Scale.Min = xleft;
      Sop1.XAxis.Scale.Max = xright;

      //tbxAusg.AppendText("i="+i+"; NowTick="+NowTick.ToString(@"ss\,fff")+Environment.NewLine);
      //tbxAusg.ScrollToEnd();

      i++;

      if(i>=Anz) {
        dpTimer.Stop();
        StoppUhr.Stop();

        tbxAusg.AppendText("FERTIG:"+Environment.NewLine+ResultText(StoppUhr.ElapsedMilliseconds, i)+Environment.NewLine);
        tbxAusg.ScrollToEnd();
      }
    }

    public void DpTimerXscrollStopp() {
      if(dpTimer!=null && dpTimer.IsEnabled) {
        dpTimer.Stop();
        StoppUhr.Stop();
      }
    }
    #endregion DpTimerXscroll

    #region SupportMethoden
    public string ResultText(long zeitMillisec, int anz) {
      return "t Ges: "+zeitMillisec+" ms; t INT AVG: "+(((double)zeitMillisec)/(anz-1)).ToString("F1")+" ms.";
    }

    public int LeseTextBoxInt(TextBox textBoxName, int? unterGrenze = null) {
      bool Pok;
      int  Erg;
      Pok = int.TryParse(textBoxName.Text, out Erg);
      if(!Pok) {
        // Keine gültige Zahl, funktioniert nur bei double, nicht bei int
        Erg = 0;
        textBoxName.Background = Brushes.LightPink;
      } else {
        if(unterGrenze!=null && Erg<unterGrenze) {
          textBoxName.Background = Brushes.LightPink;
        } else {
          textBoxName.Background = Brushes.White;
        }
      }
      return Erg;
    }
    #endregion SupportMethoden

  }
}
