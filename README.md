# CSharp Gui Performance

## Übersicht

In dieser Visual Studio Projektmappe befinden sich verschiedene Projekte zum Testen der GUI-Performance in C-Sharp.

Die ersten Projekte sind in WPF implementiert und messen timer-getriggerte Ausgaben in einer TextBox, in einem SimpleOxyPlot und in beidem.

Später werden evtl. weitere Projekte hinzugefügt, z.B. um die Performance von WinForms-Anwendungen zu testen.



## Messergebnisse

### Dell Notebook Georg Braun mit Core i7 und Win10LTSB

* Die TextBox-Ausgaben (ohne SimpleOxyPlot) erfolgen mit einem minimalen Zeitintervall von 15 ms.
  * Dabei ist die Append()-Methode der Textbox immer gleich schnell, ein manuelles Zusammenkopieren der Text-Teile mit Hilfe des „+“-Operators wird mit zunehmender Zeilenanzahl immer langsamer.

* Mit einem kleinen SOP erfolgen die Ausgaben ebenfalls mit minimal 15 ms, gemessen bis ca. 1000 Punkte.
  * Wenn man das Fenster (und damit den SOP) vergrößert, erhöht sich das Intervall auf 45-50 ms.

* Bei 10000 Punkten bleibt das Intervall bei kleinem SOP bei 15 ms, wenn man auf der x-Achse nur 20 Punkte zulässt (und den Rest nach links wegscrollen lässt).
  * Wenn alle Punkte angezeigt werden, erhöht sich das Intervall auf 33 ms (erkennen kann man dann ohnehin nichts mehr, weil die gesamte Diagrammfläche „vollgemalt“ ist).

* Es scheint keinen Unterschied zu geben zwischen
  * Programm in Debug-Konfiguration über VS2017 gestartet
  * Programm in Release-Konfiguration compiliert und über Dateimanager gestartet (Doppelklick auf exe-Datei)
